import 'package:flutter/material.dart';
import 'package:tomisha/desktop_home.dart';
import 'package:tomisha/login_bar.dart';

class DesktopLayout extends StatelessWidget {
  DesktopLayout({Key? key}) : super(key: key);

  final scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        DesktopHome(scrollController: scrollController),
        const Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: LoginBar(),
        ),
      ],
    );
  }
}