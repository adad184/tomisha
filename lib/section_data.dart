class SectionData {
  final String title;
  final String titleSection1;
  final String titleSection2;
  final String titleSection3;

  final String svgSection1;
  final String svgSection2;
  final String svgSection3;

  SectionData({
    required this.title,
    required this.titleSection1,
    required this.titleSection2,
    required this.titleSection3,
    required this.svgSection1,
    required this.svgSection2,
    required this.svgSection3,
    }
  );
}


final sectionData = [
  SectionData(
    title: "Drei einfache Schritte zu deinem neuen Job",
    titleSection1: "Drei einfache Schritte zu deinem neuen Job",
    titleSection2: "Erstellen dein Lebenslauf",
    titleSection3: "Mit nur einem Klick bewerben",
    svgSection1: "assets/svg/undraw_Profile_data_re_v81r.svg",
    svgSection2: "assets/svg/undraw_task_31wc.svg",
    svgSection3: "assets/svg/undraw_personal_file_222m.svg",
  ),
  SectionData(
    title: "Drei einfache Schritte zu deinem neuen Mitarbeiter",
    titleSection1: "Erstellen dein Unternehmensprofil",
    titleSection2: "Erstellen ein Jobinserat",
    titleSection3: "Wähle deinen neuen Mitarbeiter aus",
    svgSection1: "assets/svg/undraw_Profile_data_re_v81r.svg",
    svgSection2: "assets/svg/undraw_about_me_wa29.svg",
    svgSection3: "assets/svg/undraw_swipe_profiles1_i6mr.svg",
  ),
  SectionData(
    title: "Drei einfache Schritte zur Vermittlung neuer Mitarbeiter",
    titleSection1: "Erstellen dein Unternehmensprofil",
    titleSection2: "Erhalte Vermittlungs- angebot von Arbeitgeber",
    titleSection3: "Vermittlung nach Provision oder Stundenlohn",
    svgSection1: "assets/svg/undraw_Profile_data_re_v81r.svg",
    svgSection2: "assets/svg/undraw_job_offers_kw5d.svg",
    svgSection3: "assets/svg/undraw_business_deal_cpi9.svg",
  ),
];