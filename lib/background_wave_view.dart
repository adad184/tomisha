import 'package:flutter/material.dart';

class BackgroundWaveView extends StatelessWidget {
  const BackgroundWaveView({
    Key? key,
    this.top = true,
    this.bottom = true,
  }) : super(key: key);

  final bool top;
  final bool bottom;

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      size: const Size(double.infinity, 100),
      painter: WavePainter(
        top: top,
        bottom: bottom,
      ),
    );
  }
}

class WavePainter extends CustomPainter {
  final bool top;
  final bool bottom;

  WavePainter({
    required this.top,
    required this.bottom,
  });

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..shader = const LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          Color(0xFFE6FFFA),
          Color(0xFFEBF4FF),
        ],
      ).createShader(Offset.zero & size);

    var path = Path();

    if (top) {
      path.moveTo(0, size.height * 0.12);
      path.quadraticBezierTo(size.width * 0.2, size.height * 0.1,
          size.width * 0.4, size.height * 0.05);
      path.quadraticBezierTo(size.width * 0.9, size.height * 0.0,
          size.width * 1.0, size.height * 0.2);
      path.lineTo(size.width, size.height * 0.9);
    } else {
      path.moveTo(0, 0);
      path.lineTo(size.width, 0);
      path.lineTo(size.width, size.height * 0.9);
    }

    if (bottom) {
      path.quadraticBezierTo(size.width * 0.8, size.height * 0.9,
          size.width * 0.7, size.height * 0.95);
      path.quadraticBezierTo(size.width * 0.5, size.height * 1.0,
          size.width * 0.2, size.height * 0.95);
      path.quadraticBezierTo(size.width * 0.1, size.height * 0.9,
          size.width * 0.0, size.height * 1.0);
      path.lineTo(0, size.height);
    } else {
      path.lineTo(size.width, size.height);
      path.lineTo(0, size.height);
    }

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
