import 'package:flutter/material.dart';
import 'package:tomisha/background_gradient_view.dart';

class LoginBar extends StatelessWidget {
  const LoginBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(12),
              bottomRight: Radius.circular(12),
            ),
            boxShadow: [
              BoxShadow(
                color: Color(0x29000000),
                blurRadius: 6,
                offset: Offset(0, 3),
              )
            ],
          ),
          child: SafeArea(
            bottom: false,
            child: Container(
              height: 67,
              padding: const EdgeInsets.symmetric(horizontal: 17),
              child: const Align(
                alignment: Alignment.centerRight,
                child: Text(
                  "Login",
                  style: TextStyle(
                    color: Color(0xFF319795),
                  ),
                ),
              ),
            ),
          ),
        ),
        const Positioned(
          top: 0,
          left: 0,
          right: 0,
          height: 5,
          child: BackgroundGradientView(),
        ),
      ],
    );
  }
}
