import 'package:flutter/material.dart';

class HomeTabBar extends StatefulWidget {
  const HomeTabBar({Key? key, this.onSelect}) : super(key: key);

  final ValueChanged<int>? onSelect;

  @override
  State<HomeTabBar> createState() => _HomeTabBarState();
}

class _HomeTabBarState extends State<HomeTabBar> {
  int _selectedIndex = 0;
  final _scrollController = ScrollController();

  final _buttonTitles = [
    "Arbeitnehmer",
    "Arbeitgeber",
    "Temporärbüro",
  ];

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    bool isMobile = size.width <= 600;

    final buttons = _buttonTitles
        .mapWithIndex((e, index) => HomeTabBarButton(
              isSelected: _selectedIndex == index,
              title: e,
              showLeftRoundedCorner: index == 0,
              showRightRoundedCorner: index == 2,
              onSelect: () {
                setState(() {
                  _selectedIndex = index;

                  if (isMobile) {
                    var offset = 0.0;

                      final screenWidth = size.width;
                    if ( index == 1) {
                      offset = 280 - screenWidth / 2;
                    }

                    if ( index == 2) {
                      offset = 560 - screenWidth;
                    }

                    _scrollController.animateTo(
                      offset,
                      duration: const Duration(milliseconds: 300),
                      curve: Curves.easeInOut,
                    );
                  }
                });
                widget.onSelect?.call(index);
              },
            ))
        .toList();

    return SizedBox(
      height: 40,
      child: ListView(
        physics: const NeverScrollableScrollPhysics(),
        controller: _scrollController,
        padding: const EdgeInsets.symmetric(horizontal: 40),
        scrollDirection: Axis.horizontal,
        children: buttons,
      ),
    );
  }
}

class HomeTabBarButton extends StatelessWidget {
  const HomeTabBarButton({
    Key? key,
    required this.isSelected,
    required this.title,
    required this.showLeftRoundedCorner,
    required this.showRightRoundedCorner,
    this.onSelect,
  }) : super(key: key);
  final VoidCallback? onSelect;
  final bool isSelected;
  final String title;
  final bool showLeftRoundedCorner;
  final bool showRightRoundedCorner;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onSelect,
      child: Container(
        width: 160,
        height: 40,
        decoration: BoxDecoration(
          color: isSelected ? const Color(0xFF81E6D9) : Colors.white,
          border: Border.all(
            width: 1,
            color:
                isSelected ? const Color(0xFF81E6D9) : const Color(0xFFCBD5E0),
          ),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(showLeftRoundedCorner ? 12 : 0),
            topRight: Radius.circular(showRightRoundedCorner ? 12 : 0),
            bottomLeft: Radius.circular(showLeftRoundedCorner ? 12 : 0),
            bottomRight: Radius.circular(showRightRoundedCorner ? 12 : 0),
          ),
        ),
        child: Center(
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: isSelected ? Colors.white : const Color(0xFF319795),
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
    );
  }
}

extension IterableExtension<E> on Iterable<E> {
  Iterable<T> mapWithIndex<T>(T Function(E e, int index) f) {
    var index = -1;
    return map((item) {
      return f(item, ++index);
    });
  }
}
