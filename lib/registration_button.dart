import 'package:flutter/material.dart';
import 'package:tomisha/background_gradient_view.dart';

class Registrationbutton extends StatelessWidget {
  const Registrationbutton({
    Key? key,
    this.onTap,
  }) : super(key: key);

  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Stack(
        children: const [
          BackgroundGradientView(cornerRadius: 12),
          Center(child: Text(
            "Kostenlos Registrieren",
            style: TextStyle(
              fontSize: 14,
              color: Color(0xFFE6FFFA),
            ),
          ),),
        ],
      ),
    );
  }
}
