import 'package:flutter/material.dart';

import 'desktop_layout.dart';
import 'mobile_layout.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;

    bool isMobile = size.width <= 600;

    return Scaffold(body: isMobile ? MobileLayout() :  DesktopLayout());
  }
}
