import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tomisha/background_wave_view.dart';
import 'package:tomisha/home_tab_bar.dart';
import 'package:tomisha/registration_button.dart';
import 'package:tomisha/section_data.dart';

class DesktopHome extends StatefulWidget {
  const DesktopHome({Key? key, required this.scrollController})
      : super(key: key);

  final ScrollController scrollController;

  @override
  State<DesktopHome> createState() => _DesktopHomeState();
}

class _DesktopHomeState extends State<DesktopHome> {
  var _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final data = sectionData[_selectedIndex];

    return ListView(
      controller: widget.scrollController,
      children: [
        const _SectionHeader(),
        _SectionTabbar(onSelect: (index) {
          setState(() {
            _selectedIndex = index;
          });
        }),
        _SectionTitle(title: data.title),
        _Section1(title: data.titleSection1, svg: data.svgSection1),
        _Section2(title: data.titleSection2, svg: data.svgSection2),
        _Section3(title: data.titleSection3, svg: data.svgSection3),
        const SizedBox(height: 100),
      ],
    );
  }
}

class _SectionHeader extends StatelessWidget {
  const _SectionHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const Positioned.fill(child: BackgroundWaveView(top: false)),
        Column(
          children: [
            const SizedBox(height: 100),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 320,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        "Deine Job website",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 42,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      SizedBox(height: 50),
                      SizedBox(height: 40, child: Registrationbutton()),
                    ],
                  ),
                ),
                const SizedBox(width: 80),
                ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(455)),
                  child: Container(
                    color: Colors.white,
                    width: 455,
                    height: 455,
                    child: SvgPicture.asset(
                      "assets/svg/undraw_agreement_aajr.svg",
                      width: MediaQuery.of(context).size.width,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                )
              ],
            ),
            const SizedBox(height: 100),
          ],
        ),
      ],
    );
  }
}

class _SectionTabbar extends StatelessWidget {
  const _SectionTabbar({Key? key, this.onSelect}) : super(key: key);

  final ValueChanged<int>? onSelect;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30, bottom: 10),
      child: Center(
        child: SizedBox(
          width: 560,
          child: HomeTabBar(
            onSelect: (index) {
              onSelect?.call(index);
            },
          ),
        ),
      ),
    );
  }
}

class _SectionTitle extends StatelessWidget {
  const _SectionTitle({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 30),
      child: Align(
        alignment: Alignment.center,
        child: Container(
          constraints: const BoxConstraints(maxWidth: 400),
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 40,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
    );
  }
}

class _Section1 extends StatelessWidget {
  const _Section1({Key? key, required this.title, required this.svg})
      : super(key: key);

  final String title;
  final String svg;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 30),
      height: 200,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            textBaseline: TextBaseline.ideographic,
            children: [
              const Text(
                "1.",
                style: TextStyle(
                  color: Color(0xFF718096),
                  fontSize: 130,
                ),
              ),
              const SizedBox(width: 30),
              Container(
                padding: const EdgeInsets.only(bottom: 25),
                constraints: const BoxConstraints(maxWidth: 240),
                child: Text(
                  title,
                  maxLines: 3,
                  style: const TextStyle(
                    color: Color(0xFF718096),
                    fontSize: 16,
                  ),
                ),
              ),
              const SizedBox(width: 30),
            ],
          ),
          SizedBox(
            height: 160,
            child: SvgPicture.asset(svg, fit: BoxFit.fitWidth),
          ),
          const SizedBox(width: 100),
        ],
      ),
    );
  }
}

class _Section2 extends StatelessWidget {
  const _Section2({Key? key, required this.title, required this.svg})
      : super(key: key);

  final String title;
  final String svg;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const Positioned.fill(child: BackgroundWaveView()),
        Container(
          padding: const EdgeInsets.symmetric(vertical: 40),
          height: 200,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 160,
                child: SvgPicture.asset(svg, fit: BoxFit.fitWidth),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                textBaseline: TextBaseline.ideographic,
                children: [
                  const SizedBox(width: 30),
                  const Text(
                    "2.",
                    style: TextStyle(
                      color: Color(0xFF718096),
                      fontSize: 130,
                    ),
                  ),
                  const SizedBox(width: 30),
                  Container(
                    padding: const EdgeInsets.only(bottom: 25),
                    constraints: const BoxConstraints(maxWidth: 240),
                    child: Text(
                      title,
                      maxLines: 3,
                      style: const TextStyle(
                        color: Color(0xFF718096),
                        fontSize: 16,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class _Section3 extends StatelessWidget {
  const _Section3({Key? key, required this.title, required this.svg})
      : super(key: key);

  final String title;
  final String svg;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 30),
      height: 200,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(width: 80),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            textBaseline: TextBaseline.ideographic,
            children: [
              const Text(
                "3.",
                style: TextStyle(
                  color: Color(0xFF718096),
                  fontSize: 130,
                ),
              ),
              const SizedBox(width: 30),
              Container(
                padding: const EdgeInsets.only(bottom: 25),
                constraints: const BoxConstraints(maxWidth: 240),
                child: Text(
                  title,
                  maxLines: 3,
                  style: const TextStyle(
                    color: Color(0xFF718096),
                    fontSize: 16,
                  ),
                ),
              ),
              const SizedBox(width: 30),
            ],
          ),
          SizedBox(
            height: 160,
            child: SvgPicture.asset(svg, fit: BoxFit.fitWidth),
          ),
        ],
      ),
    );
  }
}
