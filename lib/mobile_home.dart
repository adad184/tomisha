import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tomisha/background_wave_view.dart';
import 'package:tomisha/home_tab_bar.dart';
import 'package:tomisha/section_data.dart';

class MobileHome extends StatefulWidget {
  const MobileHome({Key? key, required this.scrollController}) : super(key: key);

  final ScrollController scrollController;

  @override
  State<MobileHome> createState() => _MobileHomeState();
}

class _MobileHomeState extends State<MobileHome> {
  var _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final data = sectionData[_selectedIndex];

    return ListView(
      controller: widget.scrollController,
      children: [
        const _SectionHeader(),
        _SectionTabbar(onSelect: (index) {
          setState(() {
            _selectedIndex = index;
          });
        }),
        _SectionTitle(title: data.title),
        _Section1(title: data.titleSection1, svg: data.svgSection1),
        _Section2(title: data.titleSection2, svg: data.svgSection2),
        _Section3(title: data.titleSection3, svg: data.svgSection3),
        const SizedBox(height: 100),
      ],
    );
  }
}

class _SectionHeader extends StatelessWidget {
  const _SectionHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 100),
        Align(
          alignment: Alignment.center,
          child: Container(
            constraints: const BoxConstraints(maxWidth: 300),
            child: const Text(
              "Deine Job website",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: 42,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
        SvgPicture.asset(
          "assets/svg/undraw_agreement_aajr.svg",
          width: MediaQuery.of(context).size.width,
          fit: BoxFit.fitWidth,
        ),
      ],
    );
  }
}

class _SectionTabbar extends StatelessWidget {
  const _SectionTabbar({Key? key, this.onSelect}) : super(key: key);

  final ValueChanged<int>? onSelect;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 30, bottom: 30),
      child: HomeTabBar(
        onSelect: (index) {
          onSelect?.call(index);
        },
      ),
    );
  }
}

class _SectionTitle extends StatelessWidget {
  const _SectionTitle({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 40),
      child: Align(
        alignment: Alignment.center,
        child: Container(
          constraints: const BoxConstraints(maxWidth: 320),
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 21,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
    );
  }
}

class _Section1 extends StatelessWidget {
  const _Section1({Key? key, required this.title, required this.svg})
      : super(key: key);

  final String title;
  final String svg;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 100),
              child: Align(
                alignment: Alignment.center,
                child: SvgPicture.asset(
                  svg,
                ),
              ),
            ),
          ],
        ),
        Positioned(
          left: 40,
          right: 0,
          bottom: 0,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            textBaseline: TextBaseline.ideographic,
            children: [
              const Text(
                "1.",
                style: TextStyle(
                  color: Color(0xFF718096),
                  fontSize: 130,
                ),
              ),
              const SizedBox(width: 30),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 25),
                  child: Text(
                    title,
                    maxLines: 3,
                    style: const TextStyle(
                      color: Color(0xFF718096),
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
              const SizedBox(width: 80),
            ],
          ),
        )
      ],
    );
  }
}

class _Section2 extends StatelessWidget {
  const _Section2({Key? key, required this.title, required this.svg})
      : super(key: key);

  final String title;
  final String svg;


  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [

        const Positioned.fill(child: BackgroundWaveView()),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 80),
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 150),
                child: Align(
                  alignment: Alignment.center,
                  child: SvgPicture.asset(
                    svg,
                  ),
                ),
              ),
              Positioned(
                left: 80,
                right: 0,
                top: 0,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  textBaseline: TextBaseline.ideographic,
                  children: [
                    const Text(
                      "2.",
                      style: TextStyle(
                        color: Color(0xFF718096),
                        fontSize: 130,
                      ),
                    ),
                    const SizedBox(width: 30),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 25),
                        child: Text(
                          title,
                          maxLines: 3,
                          style: const TextStyle(
                            color: Color(0xFF718096),
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 80),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}

class _Section3 extends StatelessWidget {
  const _Section3({Key? key, required this.title, required this.svg})
      : super(key: key);

  final String title;
  final String svg;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 150),
              child: Align(
                alignment: Alignment.center,
                child: SvgPicture.asset(
                  svg,
                ),
              ),
            ),
          ],
        ),
        Positioned(
          left: 120,
          right: 0,
          top: 0,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            textBaseline: TextBaseline.ideographic,
            children: [
              const Text(
                "3.",
                style: TextStyle(
                  color: Color(0xFF718096),
                  fontSize: 130,
                ),
              ),
              const SizedBox(width: 30),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 25),
                  child: Text(
                    title,
                    maxLines: 3,
                    style: const TextStyle(
                      color: Color(0xFF718096),
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
              const SizedBox(width: 80),
            ],
          ),
        )
      ],
    );
  }
}
