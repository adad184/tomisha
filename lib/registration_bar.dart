import 'package:flutter/material.dart';
import 'package:tomisha/registration_button.dart';

class RegistrationBar extends StatelessWidget {
  const RegistrationBar({
    Key? key,
    this.onTap,
  }) : super(key: key);

  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(12),
              topRight: Radius.circular(12),
            ),
            boxShadow: [
              BoxShadow(
                color: Color(0x29000000),
                blurRadius: 6,
                offset: Offset(0, -3),
              )
            ],
          ),
          child: SafeArea(
            top: false,
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 17, vertical: 20),
              height: 40,
              child: Registrationbutton(
                onTap: onTap,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
