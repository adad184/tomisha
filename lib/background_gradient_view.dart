import 'package:flutter/material.dart';

class BackgroundGradientView extends StatelessWidget {
  const BackgroundGradientView({
    Key? key,
    this.cornerRadius = 0,
  }) : super(key: key);

  final double cornerRadius;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(cornerRadius)),
        gradient: const LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Color(0xFF319795),
            Color(0xFF3182CE),
          ],
        ),
      ),
    );
  }
}
