import 'package:flutter/material.dart';
import 'package:tomisha/login_bar.dart';
import 'package:tomisha/mobile_home.dart';
import 'package:tomisha/registration_bar.dart';

class MobileLayout extends StatelessWidget {
  MobileLayout({Key? key}) : super(key: key);

  final scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        MobileHome(scrollController: scrollController),
        const Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: LoginBar(),
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: RegistrationBar(onTap: () {
            scrollController.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeInOut);
          },),
        ),
      ],
    );
  }
}
